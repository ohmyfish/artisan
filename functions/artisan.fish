function artisan --wraps ./artisan --description 'Alias for the artisan command'
    if test -e (pwd)/.lando.yml
    	lando php -v &> /dev/null
    	if test $status -eq 0
    	    lando artisan $argv
    	    
    	    return
        end
    end
    if test -d (pwd)/vendor/laravel/sail
        sail php -v &> /dev/null
        if test $status -eq 0
            sail artisan $argv

            return
        end 
    end
    env php ./artisan $argv
end
